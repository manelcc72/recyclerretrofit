package es.manel.fragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by manelcc on 21/01/2016.
 */
public interface RestClient {

    @GET("clientes")
    Call<List<Perfil>> getAll();
}
