package es.manel.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class RecyclderRetrofit extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private List<Perfil> body = null;


    public RecyclderRetrofit() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment RecyclderRetrofit.
     */
    // TODO: Rename and change types and number of parameters
    public static RecyclderRetrofit newInstance() {
        RecyclderRetrofit fragment = new RecyclderRetrofit();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        conexion();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //EL CONTENEDOR VIEWGROUP ESTA PROVOCANDO EL VOLCADO DE MEMORIA
        //POR LO QUE DEBEREMOS DE PASARLE EN LA CONTRUCCIÓN DE LA VISTA A
        //NULL

        View v = View.inflate(getActivity(), R.layout.fragment_main, null);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler);


        recycler(body);

        return v;
    }

    private void conexion() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://servicio-monkydevs.rhcloud.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestClient service = retrofit.create(RestClient.class);
        Call<List<Perfil>> listaperfil = service.getAll();
        listaperfil.enqueue(new Callback<List<Perfil>>() {
            @Override
            public void onResponse(Response<List<Perfil>> response) {
                Log.e("error", "enetro en ACIERTO");
                body = response.body();
                recycler(body);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("error", "enetro en FALLO");
                Toast.makeText(getActivity(), "ESTO NO VA", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void recycler(List<Perfil> body) {


        recyclerView.setHasFixedSize(true);
        adapter = new RecyclerViewAdapter(body, R.layout.row_recycler);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
