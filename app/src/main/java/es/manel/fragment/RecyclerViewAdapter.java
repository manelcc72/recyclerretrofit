package es.manel.fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by manelcc on 21/01/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    List<Perfil> body;
    int itemLayout;

    public RecyclerViewAdapter(List<Perfil> body, int row_recycler) {
        this.body = body;
        this.itemLayout = row_recycler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        holder.name.setText(body.get(position).getName());
        holder.username.setText(body.get(position).getUsername());
        holder.email.setText(body.get(position).getEmail());
        holder.phone.setText(body.get(position).getPhone());
        holder.www.setText(body.get(position).getWebsite());

    }

    @Override
    public int getItemCount() {
        if (body != null) {
            return body.size();
        } else return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, username, phone, www, email;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            username = (TextView) itemView.findViewById(R.id.username);
            phone = (TextView) itemView.findViewById(R.id.phone);
            www = (TextView) itemView.findViewById(R.id.website);
            email = (TextView) itemView.findViewById(R.id.email);


        }
    }
}
